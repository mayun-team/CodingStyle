## 缩进
 **[强制]**  使用 2 个空格作为一个缩进层级。因为 html 代码通常是使用两个空格缩进的，对于写在 html 里的 javascript 代码，如果缩进是 4 个空格，按起来会比较麻烦。

 **[强制]**  函数参数超过 4 个时需要改用对象来表达这些参数。

``` javascript
// 错误的写法
submitUserData(name, nickname, age, gender, avatarUrl, createAt);

// 正确的写法
var data = {
  name: xxxxx,
  nickname: xxxxx,
  age: xxxx,
  gender: xxxx,
  avatarUrl: xxxx,
  createdAt: xxxx
};
submitUserData(data);
```


## 每行字符数限制
 **[强制]**  每一行的代码长度限制在 80 列以内。由于已经采用 2 个空格作为一个缩进层级，如果一行代码宽度能超过 80 列，那么原因也只可能包括以下其中几种：

- 字符串太长。
- 变量名太长。
- 函数名称太长。
- 函数参数太多，参数名称过长。
- 缩进层级太深，if-else、function、for/while 等代码块嵌套层数太多。

包含以上任意一种现象的代码，都是有问题的，应该修改。

例外情况：嵌入在 JavaScript 代码中的 html 代码可以放宽此宽度限制。

[建议] 每行代码的宽度应该呈曲线变化，尽量避免宽度变化过于曲折。

## 空格
 **[强制]**  二元运算符两侧必须有一个空格，一元运算符与操作对象之间不允许有空格。

``` javascript
var a = !arr.length;
a++;
a = b + c;
```

 **[强制]**  用作代码块起始的左花括号 `{` 前必须有一个空格。

``` javascript
// good
if (condition) {

}

while (condition) {

}

function funcName() {

}

// bad
if (condition){
}

while (condition){
}

function funcName(){
}
```

 **[强制]**  `if / else / for / while / function / switch / do / try / catch / finally` 关键字后，必须有一个空格。

``` javascript
// good
if (condition) {
}

while (condition) {
}

(function () {
})();

// bad
if(condition) {
}

while(condition) {
}

(function() {
})();
```

 **[强制]**  在对象创建时，属性中的 `:` 之后必须有空格，`:` 之前不允许有空格。

``` javascript
// good
var obj = {
    a: 1,
    b: 2,
    c: 3
};

// bad
var obj = {
    a : 1,
    b:2,
    c :3
};
```

 **[强制]**  函数声明、具名函数表达式、函数调用中，函数名和 `(` 之间不允许有空格。

``` javascript
// good
function funcName() {
}

var funcName = function funcName() {
};

funcName();

// bad
function funcName () {
}

var funcName = function funcName () {
};

funcName ();
```

 **[强制]**  `,` 和 `;` 前不允许有空格。如果不位于行尾，`,` 和 `;` 后必须跟一个空格。

``` javascript
// good
callFunc(a, b);

// bad
callFunc(a , b) ;
```
 **[建议]**  限制每行代码的宽度为 80 个字符，如超过该宽度，需根据逻辑条件合理缩进。


## 命名
 **[强制]**  变量和函数使用 Camel 命名法。

理由：JavaScript 的原生函数、jQuery 的函数也是这种风格，例如：document.getElementById()、$obj.addClass()，这样做是为了保持风格统一。

``` javascript
var userData = {};

function getUserData(source) {

}
```

### 函数命名语义化

 **[强制]**  函数命名使用 动词+名词 的形式，不得使用纯名词来命名函数。

理由：方便其他人理解该函数大致是干什么的，例如：setProjectCommits()、uploadUserAvatar()、submitRegistionForm()。

例外：在应用构造函数模式创建对象时，构造函数的名称为纯名词，无需遵循上述命名规则。

```
// 错误的写法

function myFunc() {
  // xxxxxx
}

function ajaxData() {
  // xxxxxx
}

function someData() {
  // xxxxxx
}

ajaxData();
someCheck();
myFunc();
```

以上代码粗看还不知道是干嘛的，需要看函数实现代码才能直到这些函数具体是干什么的，增加代码维护成本。

[建议] 使用美元符 `$` 作为 jQuery 变量的前缀。

[建议] 为 jQuery 对象加上适当的前缀名，如按钮(btn)、输入框(input)，方便识别。

``` javascript
var $btnSubmit = $('form input[type="submit"]');
var $inputEmail = $('form input[name="email"]'); 
```

 **[强制]**  类 使用 Pascal 命名法。

``` javascript
function TextNode(value, engine) {
    this.value = value;
    this.engine = engine;
}

TextNode.prototype.clone = function () {
    return this;
};
```

## 语言特性

[建议] 在等号表达式中使用类型严格的 `===`。

理由：使用 === 可以避免等于判断中的隐式类型转换。

```  javascript
![] == 0; // true
![] === 0; // false
'0' == 0; //true
'0' === 0; //false
```

[建议] 尽可能使用简洁的表达式。

```  javascript
// 字符串为空

// good
if(!name) {
  // ...
}

// bad
if (name === '') {
  // ...
}
```

```  javascript
// 字符串非空

// good
if (name) {
    // ...
}

// bad
if (name !== '') {
    // ...
}
```

``` javascript
// 布尔不成立

// good
if (!notTrue) {
    // ......
}

// bad
if (notTrue === false) {
    // ......
}
```

[建议] 减少 switch case 语句的使用。

理由：对于分支条件比较多的代码，用 switch...case 实现起来会使代码变得冗长，而且会因忘记加 break 而使得程序流程的混乱。

``` javascript
// bad
function doAction(action) {
  switch (action) {
    case 'hack':
      return 'hack';
    break;

    case 'slash':
      return 'slash';
    break;

    case 'run':
      return 'run';
    break;

    default:
      throw new Error('Invalid action.');
    break;
  }
}


// good
function doAction(action) {
  var actions = {
    'hack': function () {
      return 'hack';
    },

    'slash': function () {
      return 'slash';
    },

    'run': function () {
      return 'run';
    }
  };
 
  if (typeof actions[action] !== 'function') {
    throw new Error('Invalid action.');
  }

  return actions[action]();
}

```


## jQuery 优化

 **[强制]**  使用单独的变量保存需要多次使用的 jQuery 对象，以减少多余的元素查询，提高性能。

``` javascript
// bad
$('input[name="username"]').val('goodname');
$('input[name="username"]').parent().removeClass('has-error');
$('input[name="username"]').next('.error-message').hide();

// good
var $inputUserName = $('input[name="username"]');
$inputUserName.val('goodname');
$inputUserName.parent().removeClass('has-error');
$inputUserName.next('.error-message').hide();
```

[建议] 对于需要在 JavaScript 中频繁操作的 DOM 元素，尽量使用 id 选择器。

理由：id 选择器的查询速度是最快的。

``` html
<form class="ui form">
  <div class="field">
    <input id="input-username" type="text">
  </div>
</form>
```

``` javascript
// good
$('#input-username').val('goodname');

// bad
$('form.ui.form .field input[type="text"]').val('goodname');
```

[建议] 为元素查询指定上下文。

理由：在不指定上下文的情况下 jQuery 会在整个文档的元素中查询，当指定上下文后，jQuery 就会在该上下文中查询元素，查询范围会缩小。

``` javascript
// bad
$('input[name="username"]');
$('input[name="password"]');

// good
var $form = $('#login-form');
$form.find('input[name="username"]');
$form.find('input[name="password"]');

// good
$('#login-form input[name="username"]');
$('#login-form input[name="password"]');
```
