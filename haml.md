假设当前的控制器名称为 users，在使用 `bin/rails generate controller users` 命令生成控制器后，会创建以下几项：

    app/assets/javascripts/users.coffee
    app/assets/stylesheets/users.scss
    app/views/users/

通常情况下，`app/views/users/` 目录下的页面所需的样式都可以写在 `app/assets/stylesheets/users.scss` 中。

如果 users 视图需要引入其它样式文件，可以在 users.scss 开头注释块中加入 `= require XXX`来引用它。

有时会出现当前视图样式被其它视图的样式覆盖的问题，可以考虑缩减 application.css 的体积，删除 application.css 开头处注释中的 ` *= require_tree .` 以取消合并所有 css，只在里面选择性的引用全局性的 css，例如：semantic-ui、bootstrap 等之类的 css 框架，然后让每个视图引入自己的样式。

## 布局

``` haml
!!! 5
%html
  %head
    %title Mysite
    = csrf_meta_tags
    = stylesheet_link_tag 'application', media: 'all', 'data-turbolinks-track': 'reload'
    = stylesheet_link_tag 'users', media: 'all', 'data-turbolinks-track': 'reload'
  %body
    = yield
    = javascript_include_tag 'application', 'data-turbolinks-track': 'reload'
    = javascript_include_tag 'users', 'data-turbolinks-track': 'reload'
    = yield :javascript
```

css 文件的引入应该放置在 `<head>` 标签里面，这样能够确保页面内容在显示出来时已经应用到了完整样式，避免在页面加载过程中页面内容会随着新加入的样式而频繁变化。

对于 javascript 文件，如果该文件内的 javascript 代码主要是处理页面逻辑的（如：初始化下拉框、选项卡等组件），则应该尽量放在文档的底部。

## 局部视图

尽量不要在局部视图中嵌入 css 代码和 javascript 代码，如果该局部视图在很多不同的视图中使用，建议将它们放到单独的文件夹内（如：components），假设该局部视图名为 `_commits.haml`，那么应该新建以下文件：

    app/assets/javascripts/components/commits.js
    app/assets/stylesheets/components/commits.scss
    app/views/components/_commits.haml

其它视图要使用该局部视图，需要在视图的 scss 和 js 文件内添加 `requrie components/commits`。