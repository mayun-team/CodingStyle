# 编码规范

## 太长不看

如果觉得文档内容太多懒得看懒得记，可以靠编辑器的功能来规范代码格式。

以 [VSCode](https://code.visualstudio.com/) 编辑器为例，先安装 eslint 扩展，然后修改编辑器配置，设置 eslint 配置文件路径为 `config/eslint.config.js`。以后编辑 JavaScript 代码，只需按照编辑器给出的提示纠正代码格式，处理掉所有错误信息即可。

## 说明

编码规范是为了项目的可持续维护而存在的，对于个人项目或一次性项目而言可能并不重要，但对于需要长期维护、多人开发的项目而言，需要靠统一的编码风格来确保代码质量和寿命，方便团队协作开发，至少在某个模块的代码被其它人接手时，他不会因为不喜欢这块代码的风格而删掉重写。

如需了解为什么需要代码规范，可百度谷歌搜索相关资料，这里不做过多说明。

## 文档

- [Haml](haml.md)
- [CSS](css.md)
- [JavaScript](javascript.md)

## 其它参考

以上文档只讲述主要的规范，如需了解更多可以查看其它文档：

- CSS: https://github.com/Zhangjd/css-style-guide
- JavaScript: https://github.com/sivan/javascript-style-guide/blob/master/es5/README.md

