## 写在开头
在其它编程语言中，大多数人会追求代码的模块化、高内聚、低耦合，然而在 CSS 中，相信有些人不会这么重视，为了实现效果而不择手段，比如：写一堆重复样式、违反语义化原则写一堆与 class 名无关的样式、用一堆晦涩的 CSS 代码实现简单的特性、甚至是用 javascript 来实现原本简单几行 css 代码就能实现的效果。其实只要对自己写的 CSS 代码多一点追求，也能让它的质量变得更好一些。

## 原则
页面布局和样式尽量使用 css 框架现有的样式来实现。

通常 css 代码可以分为两种：

- 一种是只负责布局、比如：调整元素位置、尺寸大小
- 一种是负责装饰元素的视觉效果，例如：边框、阴影、背景色

建议将布局类的 class 作为容器元素的 class，不对它添加多余的装饰类的样式，两者尽量独立开来。
``` html
<!--不好的写法-->
<div class="ui tow column grid segment">
  <div class="ui column card"></div>
  <div class="ui column card"></div>
</div>
<!--建议的写法-->
<div class="segment">
  <div class="ui tow column grid">
    <div class="column">
      <div class="ui card"></div>
    </div>
    <div class="column">
      <div class="ui card"></div>
    </div>
  </div>
</div>
```


html 元素的 style 属性中，除去 `display: none`、`background-image`、`background-color` 外，尽量不要添加其它样式，通常 css 框架自带的样式能够满足大部分需求，不应该出现个别 html 元素需要特定样式的情况，实在需要这样请将样式抽离到 css 样式表里。

``` html
<!--不好的写法-->
<div class="ui button" style="margin-right: 7px; font-size:12px; height: 24px; padding: 5px">确定</div>
<!--建议的写法-->
<div class="ui button my-button">确定</div>
<style>
.my-button {
  margin-right: 7px;
  font-size: 12px;
  height: 24px;
  padding: 5px;
}
</style>
```

这样做的目的是确保 html 代码的可读性和可维护性，当需要修改某处 html 代码时，页面出现一堆内联样式会让人找不到重点，影响阅读。如果网站页面需要改版的话，为保持风格统一还要专门处理每个页面中的内联样式。

javascript 也同理，尽量将 css 样式写成类，然后用 `xxx.addClass()` 和 `xxx.removeClass()` 方法改变元素样式。同样以页面改版为例，有时会发现页面中的某些元素的样式在特定时候会莫名其妙被改变，要处理样式差异还得设置断点进行调试，然后查找原因，这无疑增加了不必要的麻烦。

建议不要在 css 代码中使用 id 选择器，因为 id 选择器权重比 class 选择器高，可能会覆盖掉元素自带的 class 的原有样式，尤其是对于组件而言，如果很多地方需要该样式，直接为元素设置 id 会容易产生命名冲突。控制页面元素的样式应该用 class 选择器，id 只用于方便在 JavaScript 代码中操作元素。

``` css
// 错误的用法
#my-button {
    padding: 10px 15px;
}

// 正确的用法
.my-button {
    padding: 10px 15px;
}
```

建议将所有 css 代码写到 css 文件里。如果 css 代码仅作用在特定页面中，将这块 css 代码写在页面的 style 标签里也是可以的。

### 简化代码

简单的 css 问题应该尽量用简单的 css 代码解决，不建议使用复杂的 css 属性、或高级的 css 特性来解决。

调整按钮、输入框等元素的宽度时，建议优先使用 padding、line-height 属性来调整。

调整元素位置、间距时，建议优先使用 margin、padding，或者空格。

实现左右布局时，建议优先使用 float 和 margin-left 来解决，例如：

``` html
<div>
  <div class="div-left"></div>
  <div class="div-right"></div>
</div>

<style>
.div-left {
  float: left;
  width: 100px;
  height: 100px;
}
.div-right {
  margin-left: 102px;
}
</style>
```

以上代码的效果是左边元素大小固定，右边元素宽度自适应。如果需要让左右内容的高度为 100%，且内容垂直居中，可考虑使用表格布局：

``` css
.div-left, .div-right {
  display: table-cell;
  vertical-align: middle;
}
```

如果需要实现更加高级的效果，可以考虑使用 flex 布局。

## 命名
全小写字母，单词间用连字符（-）连接。理由：和 css 的属性名统一，例如：background-color、box-shadow。

``` css
.main-content
.form-control
.text-center
```

### 关注分离

class 命名遵循关注分离、松耦合的原则，同时注重易于理解。以下示例代码：

``` css
.panel
.panel-header
.panel-title
.panel-body
.panel-content
.panel-footer
```

### 语义化

建议为内容区块添加合适的 id 或 class，方便他人理解该区块内容大致是什么，也有利于覆写 css 样式。

例如以下写法，只是单纯的为了实现效果而写样式，代码可读性很差，元素样式过于依赖顺序，如果往 div 里插入新的元素会使部分元素样式失效。

``` html
<div>
   <span>Name</span>
   <span>Description</span>
   <a href="http://test.com">Homepage</a>
</div>
<style>
div span:first-child {
  font-size: 18px;
  color: #333;
}
div span::nth-child(2) {
  font-size: 14px;
  color: #666;
}
div a:last-child {
  font-size: 14px;
  color: #00abff;
  text-decoration: underline;
}
</style>
```

修改后的代码如下：

``` html
<div class="user-info">
   <span class="name">Name</span>
   <span class="description">Description</span>
   <a href="http://test.com">Homepage</a>
</div>
<style>
.user-info .name {
  font-size: 18px;
  color: #333;
}
.user-info .description {
  font-size: 14px;
  color: #666;
}
.user-info a {
  font-size: 14px;
  color: #00abff;
  text-decoration: underline;
}
</style>
```

### 选择器的书写

选择符器套在必要的情况下一般不超过三层；选择器叠加一般不多于两个，如果是覆写框架现有样式，可以放宽此限制。

虽然使用 SCSS 编写样式时可以很方便的嵌套，但建议减少多余的选择器嵌套，同时也不建议把多个选择器堆叠在一起，例如 Semantic-UI 的选择器：

``` css
.ui.form .fields.error .field .ui.selection.dropdown .menu .item:hover {
    ...
}
```

### 减少污染

css 选择器名称不能过于简单，例如：label、button、input、tag、form，最好不要与 css 框架内的 css 选择器相同。

如果需要修改现有页面元素的样式，建议新建一个选择器，然后为该选择器补充样式，覆盖原有样式。例如有以下元素：

``` html
<form id="test-form">
  <button type="submit" class="ui basic green button"></button>
</form>
```

修改 button 元素的样式的话，可以为它新建一个样式类：

``` html
<form id="test-form">
  <button type="submit" class="ui basic green button my-button"></button>
</form>
```

然后为 .my-button 添加样式：

``` css
.my-button {
  margin-top: 10px;
}
```

如果要修改的元素比较多，或者添加 class 比较麻烦，可以这样做：

``` css
#test-form .button {
  margin-top: 10px;
}
```

缩小样式的作用域，只修改 ID 为 test-form 的表单里的按钮，不影响其它地方的按钮。

建议使用唯一性较高的父级选择器，例如下面的就是一种不好的做法：

``` css
form .button {
  margin-top: 10px;
}
```

作用域虽然缩小了，但还是会影响所有的 form 表单里的按钮的样式。

### 间距

如果需要调整元素之间的水平间距，或是增加特定按钮里的文字左右间距，可以使用空格来处理，这样就不用专门写 css 代码设置元素的 margin 或 padding 了。有 `&nbsp;` 和 `&emsp;` 两种空格可以使用，`&nbsp;` 宽度为 0.5em，相当于一个字母的宽度，`&emsp;` 的宽度为 1em，等于当前设置的字体大小（font-size）。假设当前的 font-size 为 14px，那么 `&nbsp` 可增加大约 7px 的间距，而 `&emsp;` 可增加 14px 的间距。